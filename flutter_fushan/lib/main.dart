import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import 'resources/resources.dart';
import 'routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        builder: FToastBuilder(),
        debugShowMaterialGrid: false,
        theme: AppTheme.themeData,
        initialRoute: '/loginPage',
        getPages: getPages);
  }
}
