import 'package:flutter_fushan/op_control/models/models.dart';
import 'package:get/get.dart';

class OPPlanDetailController extends GetxController {
  String shiftName = '';
  String date = '';
  String lineName = '';
  String stationName = '';
  String employeeId = '';
  String employeeName = '';

  updateOPDetailController({required ProductionPlan productionPlan}) {
    shiftName = productionPlan.shift.name;
    date = productionPlan.plan.name;
    lineName = productionPlan.line.name;
    stationName = productionPlan.station.name;
    employeeId = productionPlan.employee.id;
    employeeName = productionPlan.employee.name;
    update();
  }
}
