import 'dart:convert';

import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../provider/provider.dart';

class OPListController extends GetxController {
  Future<List<Plan>> getPlan() async {
    var response = await OpControlProvider().getPlan();
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Plan.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<OPCheck>> getListIPQCCheck(
      {plan, shift, line, station, project}) async {
    try {
      Map body = {};
      if (plan != null) {
        body["plan_id"] = plan.id;
      }
      if (shift != null) {
        body["shift_id"] = shift.id;
      }
      if (line != null) {
        body["line_id"] = line.id;
      }
      if (station != null) {
        body["station_id"] = station.id;
      }

      var response = await OpControlProvider().getListIPQCCheck(body);
      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        final jsonList = jsonObj['data'];
        return OPCheck.fromJsonList(jsonList);
      } else {
        return [];
      }
    } catch (e) {
      customToastError('Check certificate exception: $e');
      return [];
    }
  }
}
