import 'dart:convert';

import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../provider/provider.dart';

class OPPlanController extends GetxController {
  Future<List<Plan>> getPlan() async {
    var response = await OpControlProvider().getPlan();
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Plan.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<ProductionPlan>> getListProductionPlan(
      {plan, shift, line, station}) async {
    try {
      Map body = {
        "plan_id": plan.id,
        "shift_id": shift.id,
        "line_id": line.id,
      };
      if (station != null) {
        body["station_id"] = station.id;
      }
      var response = await OpControlProvider().postListProductionPlan(body);
      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        final jsonList = jsonObj['data'];
        return ProductionPlan.fromJsonList(jsonList);
      } else {
        return [];
      }
    } catch (e) {
      customToastError('Check certificate exception: $e');
      return [];
    }
  }
}
