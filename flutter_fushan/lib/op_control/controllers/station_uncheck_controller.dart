import 'dart:convert';
import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../provider/provider.dart';

class OPStationUncheckController extends GetxController {
  Future<List<Shift>> getShift(String? filter) async {
    var response = await OpControlProvider().getShift(filter);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Shift.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<StationUncheck>> getListStationUncheck(
      {plan, line, shift}) async {
    try {
      var filter = {};
      if (plan != null) {
        filter["plan_id"] = plan.id;
      }

      if (line != null) {
        filter["line_id"] = line.id;
      }

      if (shift != null) {
        filter["shift_id"] = shift.id;
      }

      var response = await OpControlProvider().getListStationUncheck(filter);
      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        final jsonList = jsonObj['data'];
        return StationUncheck.fromJsonList(jsonList);
      } else {
        return [];
      }
    } catch (e) {
      customToastError('Get list station uncheck exception: $e');
      return [];
    }
  }
}
