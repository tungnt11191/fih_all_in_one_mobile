import 'dart:convert';
import 'package:flutter_fushan/home/controllers/controllers.dart';
import 'package:get/get.dart';

import '../models/models.dart';
import '../provider/provider.dart';
import '../components/components.dart';

class CheckController extends GetxController {
  Future<List<Shift>> getShift(String? filter) async {
    var response = await OpControlProvider().getShift(filter);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Shift.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<Plan>> getPlanToday() async {
    var response = await OpControlProvider().getPlanToday();
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Plan.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<OPCheck>> getListIPQCCheck({plan, shift, line, station}) async {
    try {
      Map body = {
        "plan_id": plan.id,
        "shift_id": shift.id,
        "line_id": line.id,
        "station_id": station.id,
      };

      var response = await OpControlProvider().getListIPQCCheck(body);
      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        final jsonList = jsonObj['data'];
        return OPCheck.fromJsonList(jsonList);
      } else {
        return [];
      }
    } catch (e) {
      customToastError('get list certificate exception: $e');
      return [];
    }
  }

  Future<List<ProductionPlan>> getListProductionPlan(
      {plan, shift, line, station}) async {
    try {
      Map body = {
        "plan_id": plan.id,
        "shift_id": shift.id,
        "line_id": line.id,
        "station_id": station.id,
      };
      var response = await OpControlProvider().postListProductionPlan(body);
      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        final jsonList = jsonObj['data'];
        return ProductionPlan.fromJsonList(jsonList);
      } else {
        return [];
      }
    } catch (e) {
      customToastError('Check certificate exception: $e');
      return [];
    }
  }

  Future<List<Line>> getLine(plan) async {
    Map<String, dynamic> query = {};
    if (plan != null) {
      query['plan_id'] = plan.id.toString();
    }
    var response = await OpControlProvider().getLine(query);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Line.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<Station>> getStation(String? filter, selectedPlan) async {
    Map<String, dynamic> query = {};
    if (filter!.isNotEmpty) {
      query['name'] = filter;
    }
    if (selectedPlan != null) {
      query['plan_id'] = selectedPlan.id.toString();
    }
    var response = await OpControlProvider().getStation(query);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Station.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<Station>> getStationByPlan(
      {required Plan plan, required Shift shift, required Line line}) async {
    Map<String, dynamic> query = {
      'plan_id': plan.id.toString(),
      'shift_id': shift.id.toString(),
      'line_id': line.id.toString()
    };
    var response = await OpControlProvider().getStationByPlan(query);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Station.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<List<Project>> getProject(String? filter) async {
    var response = await OpControlProvider().getProject(filter);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        return Project.fromJsonList(jsonList);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<int> opCheckCert(
      {required String employeeId,
      required Station station,
      required Plan plan}) async {
    try {
      Map body = {
        "employee_id": employeeId,
        "station_id": station.id,
        "plan_id": plan.id
      };
      var response = await OpControlProvider().postCheckCert(body);

      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        if (jsonObj['message'] == 'certificate success') {
          customToastSuccess('Certificate success');
          return 1;
        } else {
          customToastError('Certificate error!');
          return 2;
        }
      } else if (response.statusCode == 400) {
        final jsonObj = jsonDecode(response.body);
        customToastError(jsonObj['message']);
        return 0;
      } else {
        throw jsonDecode(response.body)["message"] ?? "Unknown Error Occured";
      }
    } catch (error) {
      customToastError('Check certificate exception: $error');
      return 0;
    }
  }

  Future<bool> createCheckCertificate(List<OPCheck> tableChecks,
      List<ProductionPlan> tableProductionPlan) async {
    try {
      var opCheckCreate = tableChecks.where((element) => element.id == 0);
      var listCheck =
          opCheckCreate.map((item) => OPCheck.mapFromObject(item)).toList();
      var listLeave = tableProductionPlan
          .map((item) => ProductionPlan.mapFromObject(item))
          .toList();

      HomeController homeController = Get.find();
      int createUid = homeController.userInfo.value.userId;

      // body
      Map body = {
        "create_uid": createUid,
        "checks": listCheck,
        "leaves": listLeave
      };

      var response = await OpControlProvider().postCreateCheckCert(body);

      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        customToastSuccess(
            'Create success, ${jsonObj["count"].toString()} has create');
        return true;
      } else {
        if (response.statusCode == null) {
          throw response.statusText.toString();
        }
        customToastError(
            jsonDecode(response.body)["message"] ?? "Unknown Error Occured");
        return false;
      }
    } catch (e) {
      customToastError(e.toString());
      return false;
    }
  }

  Future<bool> deleteIPQCCheck(int id) async {
    try {
      // body
      Map body = {
        "id": id,
      };
      var response = await OpControlProvider().postDeleteIPQCCheck(body);

      if (response.statusCode == 200) {
        customToastSuccess('Delete sever success');
        return true;
      } else {
        customToastError('Delete sever error');
        return false;
      }
    } catch (e) {
      customToastError('Delete sever exception $e');
      return false;
    }
  }
}
