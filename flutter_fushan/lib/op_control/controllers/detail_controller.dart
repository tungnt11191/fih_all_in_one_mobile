import 'package:get/get.dart';

class OPDetailController extends GetxController {
  String shiftName = '';
  String date = '';
  String lineName = '';
  String stationName = '';
  String employeeId = '';
  String resultCheck = '';

  updateOPDetailController(
      {required shiftName,
      required date,
      required lineName,
      required stationName,
      required employeeId,
      required bool result,}) {
    this.shiftName = shiftName;
    this.date = date;
    this.lineName = lineName;
    this.stationName = stationName;
    this.employeeId = employeeId;
    resultCheck = result ? 'Cetificate Success' : 'Cetificate error!';
    update();
  }
}
