import 'package:get/get.dart';

import '../../resources/resources.dart';

class OpControlProvider extends GetConnect {
  // Get request
  Future<Response> getLine(Map<String, dynamic> query) =>
      get(Setting.hostApi + Sever.urlLine, query: query);
  Future<Response> getStation(Map<String, dynamic> query) =>
      get(Setting.hostApi + Sever.urlStation, query: query);
  Future<Response> getStationByPlan(Map<String, dynamic> query) =>
      get(Setting.hostApi + Sever.urlStationByPlan, query: query);
  Future<Response> getPlan() => get(Setting.hostApi + Sever.urlPlan);
  Future<Response> getPlanToday() => get(Setting.hostApi + Sever.urlPlanToday);
  Future<Response> getProject(filter) =>
      get(Setting.hostApi + Sever.urlProject, query: {"filter": filter});
  Future<Response> getShift(filter) =>
      get(Setting.hostApi + Sever.urlListShift, query: {"filter": filter});

  // // Post request
  Future<Response> postCheckCert(Map data) =>
      post(Setting.hostApi + Sever.urlCert, data);
  // create data check ipqc
  Future<Response> postCreateCheckCert(Map data) =>
      post(Setting.hostApi + Sever.urlCreateCheck, data);
  Future<Response> getListIPQCCheck(body) =>
      post(Setting.hostApi + Sever.urlListIPQCCheck, body);
  Future<Response> postListProductionPlan(body) =>
      post(Setting.hostApi + Sever.urlListProductionPlan, body);
  Future<Response> getListStationUncheck(body) =>
      post(Setting.hostApi + Sever.urlListStationUncheck, body);
  Future<Response> postDeleteIPQCCheck(body) =>
      post(Setting.hostApi + Sever.urlDeleteIPQCCheck, body);
}
