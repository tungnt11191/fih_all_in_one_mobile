import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fushan/routes.dart';
import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../controllers/controllers.dart';

class OPListCheck extends StatefulWidget {
  const OPListCheck({super.key});

  @override
  State<OPListCheck> createState() => _OPListCheckState();
}

class _OPListCheckState extends State<OPListCheck> {
  OPListController opListController = Get.put(OPListController());
  OPStationUncheckController opStationUncheckController =
      Get.put(OPStationUncheckController());
  CheckController checkController = Get.put(CheckController());

  Plan? _selectedPlan;
  List<OPCheck> _ipqcChecks = <OPCheck>[];
  Shift? _selectedShift;
  Line? _selectedLine;
  Station? _selectedStation;

  Future<void> updateListIPQCChecks() async {
    List<OPCheck> ipqcChecks = await opListController.getListIPQCCheck(
        plan: _selectedPlan,
        shift: _selectedShift,
        line: _selectedLine,
        station: _selectedStation);

    setState(() {
      _ipqcChecks = ipqcChecks;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "List user checked",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(
              padding: const EdgeInsets.all(4),
              children: <Widget>[
                ///************************[Selection date]**********************************///
                const Text("Select Date"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Plan>(
                        onChanged: (value) async {
                          _selectedPlan = value;
                          await updateListIPQCChecks();
                        },
                        selectedItem: _selectedPlan,
                        asyncItems: (filter) => opListController.getPlan(),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Shift]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                const Text("Select Shift"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Shift>(
                        onChanged: (value) async {
                          _selectedShift = value;
                          await updateListIPQCChecks();
                        },
                        selectedItem: _selectedShift,
                        asyncItems: (filter) =>
                            opStationUncheckController.getShift(filter),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Line]**********************************///
                const Text("Select Line"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Line>(
                        onChanged: (value) async {
                          _selectedLine = value;
                          await updateListIPQCChecks();
                        },
                        selectedItem: _selectedLine,
                        asyncItems: (filter) =>
                            checkController.getLine(_selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Station]**********************************///
                const Text("Select Station"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Station>(
                        onChanged: (value) async {
                          _selectedStation = value;
                          await updateListIPQCChecks();
                        },
                        selectedItem: _selectedStation,
                        asyncItems: (filter) =>
                            checkController.getStation(filter, _selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: true,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[List check ipqc]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                Visibility(
                  visible: _ipqcChecks.isNotEmpty,
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _ipqcChecks.length,
                      itemBuilder: (context, index) {
                        final data = _ipqcChecks[index];
                        return Padding(
                          padding: const EdgeInsets.all(5),
                          child: ListTile(
                            onTap: () {
                              Get.put(OPDetailController())
                                  .updateOPDetailController(
                                      shiftName: data.shift.name,
                                      date: data.plan.name,
                                      lineName: data.line.name,
                                      stationName: data.station.name,
                                      employeeId: data.employeeId,
                                      result: data.resultCheck);
                              Get.toNamed(Routes.opDetailPage);
                            },
                            leading: const Icon(Icons.account_circle_rounded,
                                size: 20),
                            title: Text('Employee: ${data.employeeId}'),
                            subtitle: Text(
                                'Line: ${data.line.name} \nStation: ${data.station.name}'),
                            isThreeLine: true,
                            trailing: Text(
                              data.shift.name,
                              style: const TextStyle(fontSize: 15),
                            ),
                            tileColor:
                                data.resultCheck ? Colors.green : Colors.red,
                            titleAlignment: ListTileTitleAlignment.center,
                            textColor: Colors.white,
                            iconColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                        );
                      }),
                )
              ],
            )),
      ),
    );
  }
}
