import 'package:get/get.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

import 'package:flutter_fushan/op_control/controllers/controllers.dart';
import '../components/components.dart';
import '../models/models.dart';

class OpCheckPage extends StatefulWidget {
  const OpCheckPage({super.key});

  @override
  State<OpCheckPage> createState() => _OpCheckPageState();
}

extension Texteditingcontrollerext on TextEditingController {
  void selectAll() {
    if (text.isEmpty) return;
    selection = TextSelection(baseOffset: 0, extentOffset: text.length);
  }
}

class _OpCheckPageState extends State<OpCheckPage> {
  final _formKey = GlobalKey<FormState>();
  CheckController checkController = Get.put(CheckController());
  List<Plan> _listPlan = <Plan>[];
  List<Station> _listStation = <Station>[];
  Plan? _selectedPlan;
  Station? _selectedStation;
  Line? _selectedLine;
  Shift? _selectedShift;
  int showStatus = 0;
  TextEditingController employeeController = TextEditingController();
  List<OPCheck> _tableChecks = <OPCheck>[];
  List<ProductionPlan> _productionPlans = <ProductionPlan>[];
  bool _isReadOnlyEmployee = false;

  Future<void> updateListProductionPlan() async {
    employeeController.text = '';
    showStatus = 0;
    if (isNullAnyElement()) {
      setState(() {
        _productionPlans = [];
        _tableChecks = [];
      });
    } else {
      List<ProductionPlan> productionPlans =
          await checkController.getListProductionPlan(
        plan: _selectedPlan,
        shift: _selectedShift,
        line: _selectedLine,
        station: _selectedStation,
      );
      _productionPlans = productionPlans;
      List<OPCheck> tableChecks = await checkController.getListIPQCCheck(
        plan: _selectedPlan,
        shift: _selectedShift,
        line: _selectedLine,
        station: _selectedStation,
      );
      setState(() {
        _tableChecks = tableChecks;
      });
    }
  }

  bool isNullAnyElement() {
    if (_selectedPlan == null ||
        _selectedShift == null ||
        _selectedLine == null ||
        _selectedStation == null) {
      return true;
    }
    return false;
  }

  void setListStation() {
    if (_selectedPlan != null &&
        _selectedShift != null &&
        _selectedLine != null &&
        _selectedStation == null) {
      checkController
          .getStationByPlan(
              plan: _selectedPlan!,
              shift: _selectedShift!,
              line: _selectedLine!)
          .then((value) {
        setState(() {
          _listStation = value;
        });
      });
    } else {
      setState(() {
        _listStation = [];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "IPQC check page",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(
              padding: const EdgeInsets.all(4),
              children: <Widget>[
                ///************************[Selection Plan]**********************************///
                const Text("Select Date"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Plan>(
                        onChanged: (value) {
                          _selectedPlan = value;
                          setListStation();
                          updateListProductionPlan();
                        },
                        selectedItem: _selectedPlan,
                        items: _listPlan,
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Shift]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                const Text("Select Shift"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Shift>(
                        onChanged: (value) {
                          _selectedShift = value;
                          setListStation();
                          updateListProductionPlan();
                        },
                        selectedItem: _selectedShift,
                        asyncItems: (filter) =>
                            checkController.getShift(filter),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Line]**********************************///
                const Text("Select Line"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Line>(
                        onChanged: (value) => setState(() {
                          _selectedLine = value;
                          setListStation();
                          updateListProductionPlan();
                        }),
                        selectedItem: _selectedLine,
                        asyncItems: (filter) =>
                            checkController.getLine(_selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Station]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                const Text("Select Station"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Station>(
                        onChanged: (value) => setState(() {
                          _selectedStation = value;
                          updateListProductionPlan();
                        }),
                        selectedItem: _selectedStation,
                        items: _listStation,
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[employee]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: TextFormField(
                              readOnly: _isReadOnlyEmployee ||
                                  _productionPlans.isEmpty,
                              textInputAction: TextInputAction.send,
                              controller: employeeController,
                              onEditingComplete: () async {
                                setState(() {
                                  _isReadOnlyEmployee = true;
                                });
                                employeeController.selectAll();
                                if (employeeController.text.trim().isEmpty) {
                                  employeeController.text =
                                      employeeController.text.trim();
                                } else if (isNullAnyElement()) {
                                } else {
                                  employeeController.text =
                                      employeeController.text.trim();
                                  var valueCheck =
                                      await checkController.opCheckCert(
                                          employeeId: employeeController.text,
                                          station: _selectedStation!,
                                          plan: _selectedPlan!);
                                  showStatus = valueCheck;
                                  if (showStatus == 1 || showStatus == 2) {
                                    if (_tableChecks.any((element) =>
                                        element.employeeId ==
                                        employeeController.text)) {
                                      customToastError(
                                          "Employee Id `${employeeController.text}` has exits in table");
                                    } else if (_productionPlans
                                        .where((element) =>
                                            element.employee.id ==
                                                employeeController.text &&
                                            element.isLeave)
                                        .isNotEmpty) {
                                      customToastError(
                                          "Employee Id `${employeeController.text}` has check absent, please check");
                                    } else {
                                      setState(() {
                                        _tableChecks.add(OPCheck(
                                            id: 0,
                                            plan: _selectedPlan!,
                                            shift: _selectedShift!,
                                            employeeId: employeeController.text,
                                            line: _selectedLine!,
                                            station: _selectedStation!,
                                            resultCheck: showStatus == 1
                                                ? true
                                                : false));
                                      });
                                    }
                                  }
                                }
                                setState(() {
                                  showStatus = 0;
                                  _isReadOnlyEmployee = false;
                                  employeeController.text = '';
                                });
                                // FocusManager.instance.primaryFocus?.unfocus();
                              },
                              decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.person_2_sharp),
                                labelText: 'Input Employee',
                                suffixIcon: customVisibleWidget(showStatus),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please, Enter Employee ID';
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[table plan]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                const Text('Production Plan'),
                Visibility(
                  visible: _productionPlans.isNotEmpty,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columns: const <DataColumn>[
                        DataColumn(label: Text('Employee Id')),
                        DataColumn(label: Text('Is Absent')),
                        DataColumn(label: Text('Station')),
                      ],
                      rows: _productionPlans
                          .map(
                            (item) => DataRow(
                              cells: [
                                DataCell(Text(item.employee.id)),
                                DataCell(Checkbox(
                                  value: item.isLeave,
                                  onChanged: (bool? value) {
                                    if (value! &&
                                        _tableChecks
                                            .where((element) =>
                                                element.employeeId ==
                                                item.employee.id)
                                            .isNotEmpty) {
                                      customToastError(
                                          'Employee id: ${item.employee.id} had verify, please check!');
                                      return;
                                    }
                                    setState(() {
                                      item.isLeave = value;
                                    });
                                  },
                                )),
                                DataCell(Text(item.station.name)),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),

                ///************************[table check]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                const Text('Table check certificate'),
                Visibility(
                  visible: _tableChecks.isNotEmpty,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columns: const <DataColumn>[
                        DataColumn(label: Text('Employee Id')),
                        DataColumn(label: Text('Status Check')),
                        DataColumn(label: Text('Delete')),
                      ],
                      rows: _tableChecks
                          .map(
                            (item) => DataRow(
                              cells: [
                                DataCell(Text(item.employeeId)),
                                DataCell(item.resultCheck
                                    ? const Icon(
                                        Icons.check_circle,
                                        color: Colors.green,
                                        size: 30,
                                      )
                                    : const Icon(
                                        Icons.warning_amber,
                                        color: Colors.red,
                                        size: 30,
                                      )),
                                DataCell(onTap: () async {
                                  if (item.id == 0) {
                                    setState(() {
                                      _tableChecks.remove(item);
                                    });
                                  } else {
                                    var result = await checkController
                                        .deleteIPQCCheck(item.id);
                                    if (result) {
                                      setState(() {
                                        _tableChecks.remove(item);
                                      });
                                    }
                                  }
                                }, const Icon(Icons.delete)),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),

                ///************************[Button Submit]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                Visibility(
                  visible:
                      (_tableChecks.isNotEmpty || _productionPlans.isNotEmpty),
                  child: Row(
                    children: [
                      Expanded(
                          child: ElevatedButton(
                        onPressed: () async {
                          if (_tableChecks.length +
                                  _productionPlans
                                      .where((element) => element.isLeave)
                                      .length <
                              _productionPlans.length) {
                            customToastError(
                                'Check certificate quantity must equal plan quantity');
                          } else {
                            bool result =
                                await checkController.createCheckCertificate(
                                    _tableChecks, _productionPlans);
                            if (result) {
                              setNextStation();
                            }
                            updateListProductionPlan();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.green),
                        child: const Text(
                          'Submit',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ))
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  void setNextStation() {
    if (_selectedStation == null ||
        _selectedStation == _listStation[_listStation.length - 1]) {
      return;
    }
    final index = _listStation.indexOf(_selectedStation!);
    _selectedStation = _listStation[index + 1];
  }

  @override
  void initState() {
    super.initState();
    checkController.getPlanToday().then((value) {
      if (value.isEmpty) {
        customToastError('List Plan today is empty please call `IE` check!');
      }
      _listPlan = value;
      setState(() {
        _selectedPlan = _listPlan[0];
      });
    });
  }
}
