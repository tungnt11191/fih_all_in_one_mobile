import 'package:flutter/material.dart';

import '../components/components.dart';

class OpHomePage extends StatelessWidget {
  const OpHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "OP control Home Page",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: opGridLayout(),
    );
  }
}
