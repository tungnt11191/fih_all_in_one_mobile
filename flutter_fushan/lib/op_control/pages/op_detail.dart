import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/controllers.dart';
import '../components/components.dart';

class OPDetailPage extends StatefulWidget {
  const OPDetailPage({super.key});

  @override
  State<OPDetailPage> createState() => _OPDetailStatePage();
}

class _OPDetailStatePage extends State<OPDetailPage> {
  @override
  Widget build(BuildContext context) {
    final OPDetailController opDetailController = Get.put(OPDetailController());
    return Scaffold(
        appBar: AppBar(
          title: const Text('Detail'),
          centerTitle: true,
        ),
        body: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              informationWidget(
                  label: 'Shift', content: opDetailController.shiftName),
              const Divider(),
              informationWidget(
                  label: 'Date', content: opDetailController.date),
              const Divider(),
              informationWidget(
                  label: 'Employee ID', content: opDetailController.employeeId),
              const Divider(),
              informationWidget(
                  label: 'Line Name', content: opDetailController.lineName),
              const Divider(),
              informationWidget(
                  label: 'Station Name',
                  content: opDetailController.stationName),
              const Divider(),
              informationWidget(
                  label: 'Result Check',
                  content: opDetailController.resultCheck),
              const Divider(),
            ],
          ),
        ));
  }
}
