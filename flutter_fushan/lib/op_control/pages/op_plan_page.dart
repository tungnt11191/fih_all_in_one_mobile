import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fushan/routes.dart';
import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../controllers/controllers.dart';

class OPPlanPage extends StatefulWidget {
  const OPPlanPage({super.key});

  @override
  State<OPPlanPage> createState() => _OPPlanPageState();
}

class _OPPlanPageState extends State<OPPlanPage> {
  OPPlanController opPlanController = Get.put(OPPlanController());
  OPListController opListController = Get.put(OPListController());
  OPStationUncheckController opStationUncheckController =
      Get.put(OPStationUncheckController());
  CheckController checkController = Get.put(CheckController());

  Plan? _selectedPlan;
  List<ProductionPlan> _productionPlans = <ProductionPlan>[];
  Shift? _selectedShift;
  Line? _selectedLine;
  Station? _selectedStation;

  Future<void> updateListProductionPlan() async {
    if (_selectedPlan == null ||
        _selectedShift == null ||
        _selectedLine == null) {
      setState(() {
        _productionPlans = [];
      });
    } else {
      List<ProductionPlan> productionPlans =
          await opPlanController.getListProductionPlan(
              plan: _selectedPlan,
              shift: _selectedShift,
              line: _selectedLine,
              station: _selectedStation);
      setState(() {
        _productionPlans = productionPlans;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "OP Plan",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(
              padding: const EdgeInsets.all(4),
              children: <Widget>[
                ///************************[Selection Line]**********************************///
                const Text("Select Date"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Plan>(
                        onChanged: (value) async {
                          _selectedPlan = value;
                          await updateListProductionPlan();
                        },
                        selectedItem: _selectedPlan,
                        asyncItems: (filter) => opPlanController.getPlan(),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Shift]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                const Text("Select Shift"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Shift>(
                        onChanged: (value) async {
                          _selectedShift = value;
                          await updateListProductionPlan();
                        },
                        selectedItem: _selectedShift,
                        asyncItems: (filter) =>
                            opStationUncheckController.getShift(filter),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Line]**********************************///
                const Text("Select Line"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Line>(
                        onChanged: (value) async {
                          _selectedLine = value;
                          await updateListProductionPlan();
                        },
                        selectedItem: _selectedLine,
                        asyncItems: (filter) =>
                            checkController.getLine(_selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Station]**********************************///
                const Text("Select Station"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Station>(
                        onChanged: (value) async {
                          _selectedStation = value;
                          await updateListProductionPlan();
                        },
                        selectedItem: _selectedStation,
                        asyncItems: (filter) =>
                            checkController.getStation(filter, _selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: true,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[List check ipqc]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                Visibility(
                  visible: _productionPlans.isNotEmpty,
                  child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _productionPlans.length,
                      itemBuilder: (context, index) {
                        final data = _productionPlans[index];
                        return Padding(
                          padding: const EdgeInsets.all(5),
                          child: ListTile(
                            onTap: () {
                              Get.put(OPPlanDetailController())
                                  .updateOPDetailController(
                                      productionPlan: data);
                              Get.toNamed(Routes.opPlanDetailPage);
                            },
                            leading: const Icon(Icons.account_circle_rounded,
                                size: 20),
                            title: Text('Employee: ${data.employee.id}'),
                            subtitle: Text(
                                'Line: ${data.line.name} \nStation: ${data.station.name}'),
                            isThreeLine: true,
                            trailing: Text(
                              data.shift.name,
                              style: const TextStyle(fontSize: 15),
                            ),
                            titleAlignment: ListTileTitleAlignment.center,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                        );
                      }),
                )
              ],
            )),
      ),
    );
  }
}
