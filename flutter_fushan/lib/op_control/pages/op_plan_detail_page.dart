import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/controllers.dart';
import '../components/components.dart';

class OPPlanDetailPage extends StatefulWidget {
  const OPPlanDetailPage({super.key});

  @override
  State<OPPlanDetailPage> createState() => _OPPlanDetailPageState();
}

class _OPPlanDetailPageState extends State<OPPlanDetailPage> {
  @override
  Widget build(BuildContext context) {
    final OPPlanDetailController opPlanDetailController =
        Get.put(OPPlanDetailController());
    return Scaffold(
        appBar: AppBar(
          title: const Text('Detail'),
          centerTitle: true,
        ),
        body: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              informationWidget(
                  label: 'Shift', content: opPlanDetailController.shiftName),
              const Divider(),
              informationWidget(
                  label: 'Date', content: opPlanDetailController.date),
              const Divider(),
              informationWidget(
                  label: 'Employee ID',
                  content: opPlanDetailController.employeeId),
              const Divider(),
              informationWidget(
                  label: 'Employee Name',
                  content: opPlanDetailController.employeeName),
              const Divider(),
              informationWidget(
                  label: 'Line Name', content: opPlanDetailController.lineName),
              const Divider(),
              informationWidget(
                  label: 'Station Name',
                  content: opPlanDetailController.stationName),
              const Divider(),
            ],
          ),
        ));
  }
}
