import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../components/components.dart';
import '../models/models.dart';
import '../controllers/controllers.dart';

class OPStationUncheckPage extends StatefulWidget {
  const OPStationUncheckPage({super.key});

  @override
  State<OPStationUncheckPage> createState() => _OPStationUncheckPageState();
}

class _OPStationUncheckPageState extends State<OPStationUncheckPage> {
  OPStationUncheckController opStationUncheckController =
      Get.put(OPStationUncheckController());
  CheckController checkController = Get.put(CheckController());
  OPPlanController opPlanController = Get.put(OPPlanController());

  Plan? _selectedPlan;
  List<StationUncheck> _uncheckStations = <StationUncheck>[];
  Line? _selectedLine;
  Shift? _selectedShift;

  Future<void> updateUncheckStations() async {
    if (_selectedPlan == null ||
        _selectedLine == null ||
        _selectedShift == null) {
      setState(() {
        _uncheckStations = [];
      });
      return;
    }
    List<StationUncheck> uncheckStations =
        await opStationUncheckController.getListStationUncheck(
            plan: _selectedPlan, line: _selectedLine, shift: _selectedShift);

    setState(() {
      _uncheckStations = uncheckStations;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "List Station Uncheck",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: ListView(
              padding: const EdgeInsets.all(4),
              children: <Widget>[
                ///************************[Selection Plan]**********************************///
                const Text("Select Date"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Plan>(
                        onChanged: (value) async {
                          _selectedPlan = value;
                          await updateUncheckStations();
                        },
                        selectedItem: _selectedPlan,
                        asyncItems: (filter) => opPlanController.getPlan(),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Shift]**********************************///
                const Padding(padding: EdgeInsets.all(5)),
                const Text("Select Shift"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Shift>(
                        onChanged: (value) async {
                          _selectedShift = value;
                          await updateUncheckStations();
                        },
                        selectedItem: _selectedShift,
                        asyncItems: (filter) =>
                            opStationUncheckController.getShift(filter),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: true,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[Selection Line]**********************************///
                const Text("Select Line"),
                Row(
                  children: [
                    Expanded(
                      child: DropdownSearch<Line>(
                        onChanged: (value) async {
                          _selectedLine = value;
                          await updateUncheckStations();
                        },
                        selectedItem: _selectedLine,
                        asyncItems: (filter) =>
                            checkController.getLine(_selectedPlan),
                        compareFn: (i, s) => i.isEqual(s),
                        popupProps:
                            const PopupPropsMultiSelection.modalBottomSheet(
                          isFilterOnline: false,
                          showSelectedItems: true,
                          showSearchBox: true,
                          itemBuilder: customPopupItemBuilderExample,
                          favoriteItemProps: FavoriteItemProps(
                            showFavoriteItems: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                ///************************[List station unCheck]**********************************///
                const Padding(padding: EdgeInsets.all(10)),
                Visibility(
                  visible: _uncheckStations.isNotEmpty,
                  child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _uncheckStations.length,
                      itemBuilder: (context, index) {
                        final data = _uncheckStations[index];
                        return Padding(
                          padding: const EdgeInsets.all(5),
                          child: ListTile(
                            leading: const Icon(Icons.account_circle_rounded,
                                size: 20),
                            title: Text('Station: ${data.station.name}'),
                            subtitle: Text('Line: ${data.line.name}'),
                            isThreeLine: true,
                            trailing: Text(
                              data.shift.name,
                              style: const TextStyle(fontSize: 15),
                            ),
                            titleAlignment: ListTileTitleAlignment.center,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            tileColor: Colors.red,
                            textColor: Colors.white,
                          ),
                        );
                      }),
                )
              ],
            )),
      ),
    );
  }
}
