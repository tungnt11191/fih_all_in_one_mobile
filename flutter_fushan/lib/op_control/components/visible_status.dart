import 'package:flutter/material.dart';

Widget customVisibleWidget(int show) {
  switch (show) {
    case 0:
      return const Text('');
    case 1:
      return TextButton.icon(
        onPressed: () {},
        icon: const Icon(
          Icons.check_circle,
          color: Colors.green,
          size: 30,
        ),
        label: const Text('Success!'),
      );
    case 2:
      return TextButton.icon(
        onPressed: () {},
        icon: const Icon(
          Icons.warning_amber,
          color: Colors.red,
          size: 30,
        ),
        label: const Text('Error!'),
      );
    default:
      return const Text('');
  }
}
