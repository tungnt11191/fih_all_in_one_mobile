import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:flutter_fushan/routes.dart';

Widget opGridLayout() {
  return GridView.count(
    crossAxisCount: 4,
    crossAxisSpacing: 5,
    mainAxisSpacing: 5,
    children: List.generate(
      options.length,
      (index) => GridOptions(
        layout: options[index],
      ),
    ),
  );
}

class GridLayout {
  final String title;
  final IconData icon;
  final MaterialColor color;
  final GestureTapCallback? onClick;

  GridLayout(
      {required this.title,
      required this.icon,
      required this.color,
      required this.onClick});
}

List<GridLayout> options = [
  GridLayout(
      title: 'Check \ncertificate',
      icon: Icons.calculate,
      color: Colors.cyan,
      onClick: () => Get.toNamed(Routes.opCheckRoute)),
  GridLayout(
      title: 'List \ncheck',
      icon: Icons.format_list_bulleted,
      color: Colors.lightBlue,
      onClick: () => Get.toNamed(Routes.opListCheck)),
  GridLayout(
      title: 'Plan',
      icon: Icons.date_range_outlined,
      color: Colors.blueGrey,
      onClick: () => Get.toNamed(Routes.opPlanPage)),
  GridLayout(
      title: 'Station \nUncheck',
      icon: Icons.security_update_warning_outlined,
      color: Colors.pink,
      onClick: () => Get.toNamed(Routes.opStationUncheckPage)),
];

class GridOptions extends StatelessWidget {
  final GridLayout layout;
  const GridOptions({super.key, required this.layout});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: layout.onClick,
      child: Card(
        color: layout.color,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                layout.icon,
                size: 30,
                color: Colors.white,
              ),
              Expanded(
                  child: Text(
                layout.title,
                style: const TextStyle(fontSize: 14, color: Colors.white),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
