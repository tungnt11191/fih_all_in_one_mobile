import 'package:flutter/material.dart';

Widget inputWidget(
    {required label,
    required hintText,
    required controller,
    bool readOnly = false}) {
  return Container(
    padding: const EdgeInsets.all(10),
    child: TextField(
      readOnly: readOnly,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        labelText: label,
        hintText: hintText,
      ),
      controller: controller,
    ),
  );
}

Widget informationWidget({required label, required content}) {
  return Container(
    padding: const EdgeInsets.all(10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(flex: 1, child: Text(label)),
        Expanded(flex: 1, child: Text(content))
      ],
    ),
  );
}
