import 'package:flutter_fushan/op_control/models/models.dart';

class ProductionPlan {
  int id;
  Plan plan;
  Shift shift;
  Line line;
  Station station;
  Employee employee;
  bool isLeave;

  ProductionPlan({
    required this.id,
    required this.plan,
    required this.shift,
    required this.line,
    required this.station,
    required this.employee,
    required this.isLeave,
  });

  static Map<String, dynamic> mapFromObject(ProductionPlan data) {
    return {
      "id": data.id,
      "plan_id": data.plan.id,
      "shift_id": data.shift.id,
      "line_id": data.line.id,
      "station_id": data.station.id,
      "employee_id": data.employee.id,
      "is_leave": data.isLeave,
    };
  }

  factory ProductionPlan.fromJson(Map<String, dynamic> json) {
    return ProductionPlan(
      id: json['id'],
      plan: Plan(id: json['plan_id'], name: json['date']),
      shift: Shift(id: json['shift_id'], name: json['shift_name']),
      line: Line(id: json['line_id'], name: json['line_name']),
      station: Station(id: json['station_id'], name: json['station_name']),
      employee:
          Employee(id: json['employee_id'], name: json['employee_name']),
      isLeave: json['is_leave'],
    );
  }

  static List<ProductionPlan> fromJsonList(List list) {
    return list.map((item) => ProductionPlan.fromJson(item)).toList();
  }
}
