class Line {
  int id;
  String name;

  Line({required this.id, required this.name});

  factory Line.fromJson(Map<String, dynamic> json) {
    return Line(
      id: json['id'],
      name: json['line_name'],
    );
  }

  static List<Line> fromJsonList(List list) {
    return list.map((item) => Line.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(Line model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
