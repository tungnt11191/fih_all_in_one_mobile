class Shift {
  int id;
  String name;

  Shift({required this.id, required this.name});

  factory Shift.fromJson(Map<String, dynamic> json) {
    return Shift(
      id: json['id'],
      name: json['shift_name'],
    );
  }

  static List<Shift> fromJsonList(List list) {
    return list.map((item) => Shift.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(Shift model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
