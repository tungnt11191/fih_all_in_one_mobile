class Station {
  int id;
  String name;

  Station({required this.id, required this.name});

  factory Station.fromJson(Map<String, dynamic> parsedJson) {
    return Station(
      id: parsedJson['id'],
      name: parsedJson['station_name'],
    );
  }

  static List<Station> fromJsonList(List list) {
    return list.map((item) => Station.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(Station model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
