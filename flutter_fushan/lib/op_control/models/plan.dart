class Plan {
  int id;
  String name;

  Plan({required this.id, required this.name});

  factory Plan.fromJson(Map<String, dynamic> json) {
    return Plan(
      id: json['id'],
      name: json['string_date'],
    );
  }

  static List<Plan> fromJsonList(List list) {
    return list.map((item) => Plan.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(Plan model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
