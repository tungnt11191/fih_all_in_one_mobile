import 'package:flutter_fushan/op_control/models/models.dart';

class OPCheck {
  int id;
  Plan plan;
  Shift shift;
  Line line;
  Station station;
  String employeeId;
  bool resultCheck;

  OPCheck({
    required this.id,
    required this.plan,
    required this.employeeId,
    required this.line,
    required this.station,
    required this.shift,
    required this.resultCheck,
  });

  static Map<String, dynamic> mapFromObject(OPCheck data) {
    return {
      "plan_id": data.plan.id,
      "shift_id": data.shift.id,
      "line_id": data.line.id,
      "station_id": data.station.id,
      "employee_id": data.employeeId,
    };
  }

  factory OPCheck.fromJson(Map<String, dynamic> json) {
    return OPCheck(
      id: json['id'],
      plan: Plan(id: json['plan_id'], name: json['date']),
      line: Line(id: json['line_id'], name: json['line_name']),
      station: Station(id: json['station_id'], name: json['station_name']),
      employeeId: json['employee_id'],
      shift: Shift(id: json['shift_id'], name: json['shift_name']),
      resultCheck: json['result_check'],
    );
  }

  static List<OPCheck> fromJsonList(List list) {
    return list.map((item) => OPCheck.fromJson(item)).toList();
  }
}
