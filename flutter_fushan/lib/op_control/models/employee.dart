class Employee {
  String id;
  String name;

  Employee({required this.id, required this.name});

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      id: json['code'],
      name: json['line_name'],
    );
  }

  static List<Employee> fromJsonList(List list) {
    return list.map((item) => Employee.fromJson(item)).toList();
  }
}
