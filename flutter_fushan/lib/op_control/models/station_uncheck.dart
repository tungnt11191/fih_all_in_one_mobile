import 'package:flutter_fushan/op_control/models/models.dart';

class StationUncheck {
  Plan plan;
  Line line;
  Station station;
  Shift shift;

  StationUncheck({
    required this.plan,
    required this.line,
    required this.station,
    required this.shift,
  });

  factory StationUncheck.fromJson(Map<String, dynamic> json) {
    return StationUncheck(
      plan: Plan(id: json['plan_id'], name: json['date']),
      line: Line(id: json['line_id'], name: json['line_name']),
      station: Station(id: json['station_id'], name: json['station_name']),
      shift: Shift(id: json['shift_id'], name: json['shift_name']),
    );
  }

  static List<StationUncheck> fromJsonList(List list) {
    return list.map((item) => StationUncheck.fromJson(item)).toList();
  }
}
