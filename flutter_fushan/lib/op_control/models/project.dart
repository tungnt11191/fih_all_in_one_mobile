class Project {
  int id;
  String name;

  Project({required this.id, required this.name});

  factory Project.fromJson(Map<String, dynamic> parsedJson) {
    return Project(
      id: parsedJson['id'],
      name: parsedJson['project_name'],
    );
  }

  static List<Project> fromJsonList(List list) {
    return list.map((item) => Project.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(Project model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
