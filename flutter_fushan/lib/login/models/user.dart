import 'package:odoo_rpc/odoo_rpc.dart';

class User {
  int? userId;
  String? userLogin;
  String? userName;
  int? companyId;
  String? sessionId;
  String? tag;

  factory User.fromJson(OdooSession odooSession) {
    return User(
      userId: odooSession.userId,
      userLogin: odooSession.userLogin,
      userName: odooSession.userName,
      companyId: odooSession.companyId,
      sessionId: odooSession.id,
      tag: odooSession.userName[0],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'userLogin': userLogin,
      'userName': userName,
      'companyId': companyId,
      'sessionId': sessionId,
      'tag': tag
    };
  }

  User(
      {this.userId,
      this.userLogin,
      this.userName,
      this.companyId,
      this.sessionId,
      this.tag});
}
