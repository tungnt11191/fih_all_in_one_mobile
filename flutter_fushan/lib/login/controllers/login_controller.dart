import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:odoo_rpc/odoo_rpc.dart';

import '../../op_control/components/show_toast.dart';
import '../../resources/resources.dart';
import '../models/models.dart';

class LoginController extends GetxController {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  // Create storage
  final storage = const FlutterSecureStorage();

  final userInfo = User().obs;

  Future<void> loginWithUserName() async {
    final client = OdooClient(Setting.hostApi);
    if (userNameController.text.isEmpty || passwordController.text.isEmpty) {
      customToastError('User name and password required');
      throw 'User name and password required';
    }
    try {
      OdooSession response = await client.authenticate(
          Setting.db, userNameController.text, passwordController.text);

      User user = User.fromJson(response);
      userInfo.value = user;

      // write value
      await storage.write(key: 'userName', value: userNameController.text);
      await storage.write(key: 'password', value: passwordController.text);

      userNameController.clear();
      passwordController.clear();
      update();
      Get.offNamed('homePage');
    } on OdooException catch (e) {
      if (e.message.contains('{code: 200')) {
        showDialog(
            context: Get.context!,
            builder: (context) {
              return const SimpleDialog(
                title: Text('Error'),
                contentPadding: EdgeInsets.all(20),
                children: [Text('user name or password wrong')],
              );
            });
      } else {
        rethrow;
      }
    } catch (error) {
      showDialog(
          context: Get.context!,
          builder: (context) {
            return SimpleDialog(
              title: const Text('Error'),
              contentPadding: const EdgeInsets.all(20),
              children: [Text(error.toString())],
            );
          });
    }
  }

  @override
  Future<void> onInit() async {
    super.onInit();
    //   read all values
    Map<String, String> allValues = await storage.readAll();
    userNameController.text = allValues['userName']!;
    passwordController.text = allValues['password']!;
    }
}
