enum Shift {
  dayShift(id: 1, name: 'Day Shift'),
  nightShift(id: 2, name: 'Night Shift');

  const Shift({required this.id, required this.name});

  final int id;
  final String name;
  @override
  String toString() => name;
}
