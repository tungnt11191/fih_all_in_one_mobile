import 'package:flutter/material.dart';

class Sever {
  Sever._();

  static final navigationKey = GlobalKey<NavigatorState>();

  static final RegExp emailRegex = RegExp(
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.([a-zA-Z]{2,})+",
  );

  static final RegExp passwordRegex = RegExp(
    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$#!%*?&_])[A-Za-z\d@#$!%*?&_].{7,}$',
  );
  static const urlLogin = '/web/session/authenticate';
  static const urlLine = '/op-control/list-line';
  static const urlStation = '/op-control/list-station';
  static const urlStationByPlan = '/op-control/list-station-by-plan';
  static const urlPlan = '/op-control/list-plan';
  static const urlPlanToday = '/op-control/list-plan-today';
  static const urlCert = '/op-control/check-cert';
  static const urlCreateCheck = '/op-control/op-check/create';
  static const urlListIPQCCheck = '/op-control/get-list-op-check';
  static const urlListProductionPlan = '/op-control/get-list-plan';
  static const urlListStationUncheck = '/op-control/list-station-uncheck';
  static const urlListShift = '/op-control/list-shift';
  //Asset management api links
  static const urlAssetInfo = '/web/asset-management/asset-info';
  static const urlAssetLocation = '/web/asset-management/location-list';
  static const urlAssetUpdateInfo = '/web/asset-management/update-asset-info';
  static const urlProject = '/op-control/list-project';
  static const urlDeleteIPQCCheck = '/op-control/delete-op-check';
}
