class Setting {
  Setting._();

  static const addressApi = 'itportal-dev.fushan.fihnbb.com';
  static const portApi = '8888';
  static const hostApi = 'http://$addressApi:$portApi';
  static const db = 'FIH_ATTENDANCE_REPORT_TEST';
}
