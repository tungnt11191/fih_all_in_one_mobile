class User {
  late String employeeId;
  late String name;
  late String tag;

  User({required this.employeeId, required this.name, required tag});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      employeeId: parsedJson['employee_id'].toString(),
      name: parsedJson['name'].toString(),
      tag: parsedJson['name'].toString()[0],
    );
  }
}
