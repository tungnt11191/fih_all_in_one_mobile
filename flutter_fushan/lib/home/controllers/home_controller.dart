import 'package:flutter_fushan/login/controllers/controllers.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  LoginController loginController = Get.find();
  dynamic userInfo;

  @override
  void onInit() {
    super.onInit();
    userInfo = loginController.userInfo;
  }
}
