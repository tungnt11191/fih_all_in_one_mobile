import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../routes.dart';

Widget homeGridLayout() {
  return GridView.count(
    crossAxisCount: 4,
    crossAxisSpacing: 5,
    mainAxisSpacing: 5,
    scrollDirection: Axis.vertical,
    physics: const NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    children: List.generate(
      options.length,
      (index) => GridOptions(
        layout: options[index],
      ),
    ),
  );
}

class GridLayout {
  final String title;
  final IconData icon;
  final MaterialColor color;
  final GestureTapCallback? onClick;

  GridLayout(
      {required this.title,
      required this.icon,
      required this.color,
      required this.onClick});
}

List<GridLayout> options = [
  GridLayout(
      title: 'OP \nControl',
      icon: Icons.person,
      color: Colors.indigo,
      onClick: () => Get.toNamed(Routes.opHomePage)),
  // GridLayout(title: 'Home', icon: Icons.home),
  // GridLayout(title: 'Email', icon: Icons.email),
  // GridLayout(title: 'Alarm', icon: Icons.access_alarm),
  // GridLayout(title: 'Wallet', icon: Icons.account_balance_wallet),
  // GridLayout(title: 'Backup', icon: Icons.backup),
  // GridLayout(title: 'Book', icon: Icons.book),
  // GridLayout(title: 'Camera', icon: Icons.camera_alt_rounded),
  // GridLayout(title: 'Print', icon: Icons.print),
  // GridLayout(title: 'Phone', icon: Icons.phone),
  // GridLayout(title: 'Notes', icon: Icons.speaker_notes),
  // GridLayout(title: 'Music', icon: Icons.music_note_rounded),
  // GridLayout(title: 'Car', icon: Icons.directions_car),
  // GridLayout(title: 'Bicycle', icon: Icons.directions_bike),
  // GridLayout(title: 'Boat', icon: Icons.directions_boat),
  // GridLayout(title: 'Bus', icon: Icons.directions_bus),
  // GridLayout(title: 'Train', icon: Icons.directions_railway),
  // GridLayout(title: 'Walk', icon: Icons.directions_walk),
  // GridLayout(title: 'Contact', icon: Icons.contact_mail),
  // GridLayout(title: 'Duo', icon: Icons.duo),
  // GridLayout(title: 'Hour', icon: Icons.hourglass_bottom),
  // GridLayout(title: 'Mobile', icon: Icons.mobile_friendly),
  // GridLayout(title: 'Message', icon: Icons.message),
  // GridLayout(title: 'Key', icon: Icons.vpn_key),
  // GridLayout(title: 'Wifi', icon: Icons.wifi),
  // GridLayout(title: 'Bluetooth', icon: Icons.bluetooth),
  // GridLayout(title: 'Smile', icon: Icons.sentiment_satisfied),
  // GridLayout(title: 'QR', icon: Icons.qr_code),
  // GridLayout(title: 'ADD', icon: Icons.add_box),
  // GridLayout(title: 'Link', icon: Icons.link),
  GridLayout(
      title: 'Asset \nManagement',
      icon: Icons.assessment,
      color: Colors.indigo,
      onClick: () => Get.toNamed(Routes.AssetHomePage)),
];

class GridOptions extends StatelessWidget {
  final GridLayout layout;
  const GridOptions({super.key, required this.layout});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: layout.onClick,
      child: Card(
        color: layout.color,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                layout.icon,
                size: 30,
                color: Colors.white,
              ),
              Expanded(
                child: Text(
                  layout.title,
                  style: const TextStyle(fontSize: 14, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
