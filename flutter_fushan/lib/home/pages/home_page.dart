import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:flutter_fushan/home/controllers/controllers.dart';
import '../../routes.dart';
import '../components/components.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeController homeController = Get.put(HomeController());
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (index) {
        case 1:
          Get.offAllNamed(Routes.loginRoute);
          break;
        default:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: ListView(
          children: <Widget>[
            ///************************[image]**********************************///
            Row(
              children: [
                Expanded(
                    child: Container(
                  height: Get.size.height * 0.24,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/fushan.jpg"),
                          fit: BoxFit.fill)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                              radius: 20,
                              backgroundColor: const Color(0xFF5685EC),
                              child: Text(homeController.userInfo.value.tag,
                                  style: const TextStyle(color: Colors.white))),
                          const SizedBox(
                            width: 20,
                          ),
                          Text(
                            homeController.userInfo.value.userName.isEmpty
                                ? 'Welcome \n'
                                : 'Welcome \n ${homeController.userInfo.value.userName}',
                            style: const TextStyle(color: Colors.white),
                          )
                        ],
                      )
                    ],
                  ),
                ))
              ],
            ),

            ///************************[grid icon app]**********************************///
            const Padding(padding: EdgeInsets.all(10)),
            homeGridLayout()
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 24,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.logout),
            label: 'Logout',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
