import 'package:flutter_fushan/asset_management/pages/asset_filter_page.dart';
import 'package:flutter_fushan/asset_management/pages/asset_home_page.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

import 'package:flutter_fushan/login/login.dart';
import 'package:flutter_fushan/home/home.dart';
import 'package:flutter_fushan/op_control/op_control.dart';

/// Routes name to directly navigate the route by its name
class Routes {
  static const String loginRoute = '/loginPage';
  static const String homeRoute = '/homePage';
  // Op Control routes
  static const String opHomePage = '/OpHomePage';
  static const String opCheckRoute = '/OpCheckPage';
  static const String opListCheck = '/OpListCheck';
  static const String opDetailPage = '/OpDetailPage';
  static const String opPlanPage = '/OpPlanPage';
  static const String opPlanDetailPage = '/OpPlanDetailPage';
  static const String opStationUncheckPage = '/opStationUncheckPage';
  // Asset management
  static const String AssetHomePage = '/AssetHomePage';
  static const String AssetFilterPage = '/AssetFilterPage';
}

/// Add this list variable into your GetMaterialApp as the value of getPages parameter.
/// You can get the reference to the above GetMaterialApp code.
final getPages = [
  GetPage(name: Routes.loginRoute, page: () => const LoginPage()),
  GetPage(name: Routes.homeRoute, page: () => const HomePage()),
  // op Control
  GetPage(name: Routes.opHomePage, page: () => const OpHomePage()),
  GetPage(name: Routes.opCheckRoute, page: () => const OpCheckPage()),
  GetPage(name: Routes.opListCheck, page: () => const OPListCheck()),
  GetPage(name: Routes.opDetailPage, page: () => const OPDetailPage()),
  GetPage(name: Routes.opPlanPage, page: () => const OPPlanPage()),
  GetPage(name: Routes.opPlanDetailPage, page: () => const OPPlanDetailPage()),
  GetPage(
      name: Routes.opStationUncheckPage,
      page: () => const OPStationUncheckPage()),
  // Asset management
  GetPage(name: Routes.AssetHomePage, page: () => const AssetHomePage()),
  GetPage(name: Routes.AssetFilterPage, page: () => const AssetFilterPage()),
];
