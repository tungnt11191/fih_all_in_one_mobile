class AssetLocation {
  int id;
  String name;

  AssetLocation({required this.id, required this.name});

  factory AssetLocation.fromJson(Map<String, dynamic> json) {
    return AssetLocation(
      id: json['id'],
      name: json['name'],
    );
  }

  static List<AssetLocation> fromJsonList(List list) {
    return list.map((item) => AssetLocation.fromJson(item)).toList();
  }

  ///custom comparing function to check if two users are equal
  bool isEqual(AssetLocation model) {
    return id == model.id;
  }

  @override
  String toString() => name;
}
