class Asset {
  int id;
  String lot;
  String asset_serial;
  String location;
  String status;

  Asset({required this.id, required this.lot, required this.asset_serial, required this.location, required this.status});

  factory Asset.fromJson(Map<String, dynamic> json) {
    return Asset(
        id: json['id'],
        lot: json['lot'],
        asset_serial: json['serial'],
        location: json['location'],
        status: json['status']
          );
  }
  
  static List<Asset> fromJsonList(List list) {
    return list.map((item) => Asset.fromJson(item)).toList();
  }
  static Map<String, dynamic> mapFromObject (Asset table) {
    return {
      "lot": table.lot
    };
  }
  
}
