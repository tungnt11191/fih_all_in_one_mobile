import 'package:get/get.dart';

import '../../resources/resources.dart';

class AssetProvider extends GetConnect {
  //Get request
  Future<Response> getAsset(filter) {
    return get(Setting.hostApi + Sever.urlAssetInfo, query: {"filter": filter});
  }

  Future<Response> getLocationList(filter) {
    return get(Setting.hostApi + Sever.urlAssetLocation,
        query: {"filter": filter});
  }
  //Update request
   Future<Response> postAssetInfo(Map data) =>
      post(Setting.hostApi + Sever.urlAssetUpdateInfo, data);
}
