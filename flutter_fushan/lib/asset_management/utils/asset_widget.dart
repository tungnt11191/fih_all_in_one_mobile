import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'asset_colors.dart';
import 'asset_constant.dart';
import 'asset_strings.dart';

Padding AssetEditTextStyle(var hintText,
    {isPassword = true, TextInputType keyboardType = TextInputType.name}) {
  return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: TextFormField(
        style: TextStyle(fontSize: textSizeMedium, fontFamily: fontRegular),
        obscureText: isPassword,
        keyboardType: keyboardType,
        cursorColor: Colors.black,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(26, 14, 4, 14),
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.black),
          filled: true,
          fillColor: asset_edit_text_color,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: asset_edit_text_color, width: 0.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: asset_edit_text_color, width: 0.0),
          ),
        ),
      ));
}

// ignore: must_be_immutable
class AssetButton extends StatefulWidget {
  var textContent;
  VoidCallback onPressed;

  AssetButton({required this.textContent, required this.onPressed});

  @override
  State<StatefulWidget> createState() {
    return AssetButtonState();
  }
}

class AssetButtonState extends State<AssetButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(color: asset_white),
        elevation: 4,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
      ),
      onPressed: widget.onPressed,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[asset_colorPrimary, asset_color_gradient1],
          ),
          borderRadius: BorderRadius.all(Radius.circular(80.0)),
        ),
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: Text(
              widget.textContent,
              style: TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}

Widget mSale(BuildContext context) {
  return Container(
    alignment: Alignment.bottomLeft,
    margin: EdgeInsets.only(left: spacing_middle, bottom: spacing_standard_new),
    child: Container(
      padding: EdgeInsets.fromLTRB(spacing_standard_new, spacing_control_half,
          spacing_standard_new, spacing_control_half),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[asset_colorPrimary, asset_color_gradient1],
        ),
        borderRadius: BorderRadius.all(Radius.circular(80.0)),
      ),
      child: text(t13_lbl_for_sale, textColor: asset_white, fontSize: 12.0),
    ),
  );
}

var mSearch = Container(
  // decoration: boxDecoration(radius: 50, showShadow: false,bgColor: appStore.appBarColor),
  child: TextFormField(
    onTap: () {
      
    },
    textAlignVertical: TextAlignVertical.center,
    cursorColor: Colors.black,
    decoration: InputDecoration(
      filled: true,
      fillColor: asset_edit_text_color,
      hintText: t13_lbl_search,
      hintStyle: TextStyle(color: Colors.black12),
      border: InputBorder.none,
      suffixIcon: Icon(Icons.search, color: asset_color_gradient1),
      contentPadding:
          EdgeInsets.only(left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
    ),
  ),
  alignment: Alignment.center,
);

Widget text(
  String? text, {
  var fontSize = textSizeLargeMedium,
  Color? textColor,
  var fontFamily,
  var isCentered = false,
  var maxLine = 1,
  var latterSpacing = 0.5,
  bool textAllCaps = false,
  var isLongText = false,
  bool lineThrough = false,
}) {
  return Text(
    textAllCaps ? text!.toUpperCase() : text!,
    textAlign: isCentered ? TextAlign.center : TextAlign.start,
    maxLines: isLongText ? null : maxLine,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(
      fontFamily: fontFamily ?? null,
      fontSize: fontSize,
      color: textColor ?? asset_textColorSecondary,
      height: 1.5,
      letterSpacing: latterSpacing,
      decoration:
          lineThrough ? TextDecoration.lineThrough : TextDecoration.none,
    ),
  );
}

AppBar appBar(BuildContext context, String title,
    {final bool isDashboard = false,
    List<Widget>? actions,
    bool showBack = true,
    Color? color,
    Color? iconColor,
    Color? textColor,
    double? elevation}) {
  return AppBar(
    automaticallyImplyLeading: false,
    backgroundColor: color ?? asset_Grey,
    title: const Text('Asset'),
    elevation: elevation ?? 0.5,
  );
}

class MWDropDownButtonScreen extends StatefulWidget {
  const MWDropDownButtonScreen({super.key});

  @override
  _MWDropDownButtonScreenState createState() => _MWDropDownButtonScreenState();
}

class _MWDropDownButtonScreenState extends State<MWDropDownButtonScreen> {
  List<String> listOfCategory = [
    'It',
    'Computer Science',
    'Business',
    'Data Science',
    'Infromation Technologies',
    'Health',
    'Physics'
  ];
  String? selectedIndexCategory = 'Business';
  String? dropdownNames;
  String? dropdownScrollable = 'I';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, "DropDown Button"),
      body: Container(
        margin: EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Text('Category', style: TextStyle(fontSize: 10)),
              Card(
                  elevation: 4,
                  child: DropdownButton(
                    isExpanded: true,
                    dropdownColor: Colors.blueGrey,
                    value: selectedIndexCategory,
                    style: TextStyle(),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: asset_Grey,
                    ),
                    onChanged: (dynamic newValue) {
                      setState(() {
                        selectedIndexCategory = newValue;
                      });
                    },
                    items: listOfCategory.map((category) {
                      return DropdownMenuItem(
                        child: Text(category,
                                style: TextStyle(fontStyle: FontStyle.italic))
                            .paddingAll(8),
                        value: category,
                      );
                    }).toList(),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
