import 'dart:convert';
import 'package:get/get.dart';

import '../models/models.dart';
import '../providers/providers.dart';


class AssetLocationController extends GetxController {
  static Future<List<AssetLocation>> getLocationList(String? filter) async {
    var response = await AssetProvider().getLocationList(filter);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        var data = AssetLocation.fromJsonList(jsonList);
        print(data);
        return data;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
}
