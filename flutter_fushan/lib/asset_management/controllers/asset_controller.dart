import 'dart:convert';
import 'package:get/get.dart';

import '../models/models.dart';
import '../providers/providers.dart';
import 'package:flutter_fushan/op_control/components/components.dart';

class AssetController extends GetxController {
  Future<List<Asset>> getAsset(String? filter) async {
    var response = await AssetProvider().getAsset(filter);
    if (response.statusCode == 200) {
      final jsonObj = jsonDecode(response.body);
      if (jsonObj['message'] == 'success') {
        final jsonList = jsonObj['data'];
        var data = Asset.fromJsonList(jsonList);
        return data;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  //Update asset info
  Future<bool> assetUpdate(
      {required String status,
      required location,
      required List<Asset> lot}) async {
    try {
      int locationId = location.id;
      var listMap = lot.map((item) => Asset.mapFromObject(item)).toList();

      Map body = {"status": status, "location": locationId, "lot": listMap};
      var response = await AssetProvider().postAssetInfo(body);

      if (response.statusCode == 200) {
        final jsonObj = jsonDecode(response.body);
        if (jsonObj['message'] == 'update success') {
          customToastSuccess('Update success');
          return true;
        } else {
          customToastError('Update error!');
          return false;
        }
      } else if (response.statusCode == 400) {
        final jsonObj = jsonDecode(response.body);
        customToastError(jsonObj['message']);
        return false;
      } else {
        throw jsonDecode(response.body)["message"] ?? "Unknown Error Occured";
      }
    } catch (error) {
      customToastError('Update asset info exception: $error');
      return false;
    }
  }
}
