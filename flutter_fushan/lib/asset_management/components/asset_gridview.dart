import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:flutter_fushan/routes.dart';

Widget assetGridLayout() {
  return GridView.count(
    crossAxisCount: 4,
    crossAxisSpacing: 5,
    mainAxisSpacing: 5,
    children: List.generate(
      options.length,
      (index) => GridOptions(
        layout: options[index],
      ),
    ),
  );
}

class GridLayout {
  final String title;
  final IconData icon;
  final MaterialColor color;
  final GestureTapCallback? onClick;

  GridLayout(
      {required this.title,
      required this.icon,
      required this.color,
      required this.onClick});
}

List<GridLayout> options = [
  GridLayout(
      title: 'Asset \nInformation',
      icon: Icons.calculate,
      color: Colors.cyan,
      onClick: () => Get.toNamed(Routes.AssetFilterPage)),
];

class GridOptions extends StatelessWidget {
  final GridLayout layout;
  const GridOptions({super.key, required this.layout});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: layout.onClick,
      child: Card(
        color: layout.color,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                layout.icon,
                size: 30,
                color: Colors.white,
              ),
              Expanded(
                  child: Text(
                layout.title,
                style: const TextStyle(fontSize: 14, color: Colors.white),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
