import 'package:flutter/material.dart';
import 'package:flutter_fushan/asset_management/models/models.dart';

// ignore: must_be_immutable
class AssetTable extends StatelessWidget {
  // final List<Map<String, String>> data = [
  //   {
  //     'ID': '1',
  //     'Asset Number': 'A8000',
  //     'Lot/Serial': '00006',
  //     'Location': 'IT Stock'
  //   },
  //   // Add more data rows as needed
  // ];
  List<Asset> assetInfo = <Asset>[];

  AssetTable({super.key, required this.assetInfo});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          DataTable(
            columns: const [
              DataColumn(label: Text('ID')),
              DataColumn(label: Text('Asset Number')),
              DataColumn(label: Text('Lot/Serial')),
              DataColumn(label: Text('Location')),
              DataColumn(label: Text('Status')),
            ],
            rows: assetInfo.map((rowData) {
              return DataRow(cells: [
                DataCell(Text(rowData.id.toString())),
                DataCell(Text(rowData.asset_serial)),
                DataCell(Text(rowData.lot)),
                DataCell(Text(rowData.location)),
                DataCell(Text(rowData.status.toUpperCase())),
              ]);
            }).toList(),
          ),
        ],
      ),
    );
  }
}
