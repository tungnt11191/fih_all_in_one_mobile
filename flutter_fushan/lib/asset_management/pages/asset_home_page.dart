import 'package:flutter/material.dart';

import '../components/components.dart';

class AssetHomePage extends StatelessWidget {
  const AssetHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Asset Management Home Page",
          style: TextStyle(fontSize: 20),
        ),
      ),
      body: assetGridLayout(),
    );
  }
}