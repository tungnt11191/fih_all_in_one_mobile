import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:flutter_fushan/asset_management/utils/utils.dart';
import 'package:flutter_fushan/asset_management/components/components.dart';
import '../controllers/controllers.dart';
import '../models/models.dart';
import 'package:flutter_fushan/op_control/components/components.dart';

class AssetFilterPage extends StatefulWidget {
  const AssetFilterPage({super.key});

  @override
  AssetFilterPageState createState() => AssetFilterPageState();
}

class AssetFilterPageState extends State<AssetFilterPage> {
  AssetLocation? _selectLocation;
  SelectStatus? _selectStatus = SelectStatus.ok;
  TextEditingController searchController = TextEditingController();
  AssetController assetController = Get.put(AssetController());
  List<Asset> _assetInfo = <Asset>[];

  @override
  Widget build(BuildContext context) {
    Widget mLabel(String value) {
      return text(value.toUpperCase(), textColor: asset_textColorPrimary);
    }

    var mView = Container(
      margin: EdgeInsets.only(
          top: spacing_standard_new, bottom: spacing_standard_new),
      child: Divider(height: 1, color: asset_view_color),
    );

    return Scaffold(
      backgroundColor: asset_app_background,
      appBar: appBar(context, t13_lbl_filter),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            SizedBox(height: 12),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(spacing_standard_new),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        // decoration: boxDecoration(radius: 50, showShadow: false,bgColor: appStore.appBarColor),
                        alignment: Alignment.center,
                        // decoration: boxDecoration(radius: 50, showShadow: false,bgColor: appStore.appBarColor),
                        child: TextFormField(
                          controller: searchController,
                          onEditingComplete: () async {
                            var assetInfo = await assetController
                                .getAsset(searchController.text);
                            if (assetInfo.isNotEmpty) {
                              setState(() {
                                _assetInfo.addAll(assetInfo);
                                searchController.clear();
                              });
                            } else {
                              customToastError("The Asset does not found");
                            }
                            FocusManager.instance.primaryFocus?.unfocus();
                          },
                          textAlignVertical: TextAlignVertical.center,
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: asset_edit_text_color,
                            hintText: t13_lbl_search,
                            hintStyle: TextStyle(color: Colors.black12),
                            border: InputBorder.none,
                            suffixIcon: Icon(Icons.search,
                                color: asset_color_gradient1),
                            contentPadding: EdgeInsets.only(
                                left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                          ),
                        ),
                      ),
                      mView,
                      Container(
                        child: AssetTable(
                          assetInfo: _assetInfo,
                        ),
                      ),
                      mView,
                      mLabel(asset_lbl_status),
                      SizedBox(height: spacing_standard_new),
                      Row(
                        children: <Widget>[
                          SizedBox(height: 12),
                          Expanded(
                            child: ListTile(
                              title: const Text('OK'),
                              leading: Radio<SelectStatus>(
                                  groupValue: _selectStatus,
                                  value: SelectStatus.ok,
                                  onChanged: (SelectStatus? value) {
                                    setState(() {
                                      _selectStatus = value;
                                    });
                                  }),
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text('NG'),
                              leading: Radio<SelectStatus>(
                                groupValue: _selectStatus,
                                value: SelectStatus.ng,
                                onChanged: (SelectStatus? value) =>
                                    setState(() {
                                  _selectStatus = value;
                                }),
                              ),
                            ),
                          ),
                        ],
                      ),
                      mView,
                      mLabel(asset_lbl_location),
                      Row(
                        children: [
                          Expanded(
                            child: DropdownSearch<AssetLocation>(
                              onChanged: (value) => setState(() {
                                _selectLocation = value;
                              }),
                              selectedItem: _selectLocation,
                              asyncItems: (filter) =>
                                  AssetLocationController.getLocationList(
                                      filter),
                              compareFn: (i, s) => i.isEqual(s),
                              popupProps: const PopupPropsMultiSelection
                                  .modalBottomSheet(
                                isFilterOnline: true,
                                showSelectedItems: true,
                                showSearchBox: true,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: spacing_large),
                      AssetButton(
                        textContent: t13_lbl_update,
                        onPressed: () async {
                          //Submit button action here
                          if (_assetInfo.isNotEmpty) {
                            for (var element in _assetInfo) {
                              element.location = _selectLocation!.name;
                              element.status =
                                  _selectStatus!.index == 1 ? 'ok' : 'ng';
                            }
                            var result = await assetController.assetUpdate(
                                status: _selectStatus!.index == 1 ? 'ok' : 'ng',
                                location: _selectLocation,
                                lot: _assetInfo);
                            if (result) {
                              setState(() {
                                _assetInfo.clear();
                              });
                            }
                          }
                        },
                      ),
                      SizedBox(height: spacing_large),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
